import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;

public class ButtonEvent2Class extends JFrame {
    
    private JButton button;
    
    public ButtonEvent2Class(){
        button = new JButton("click mich!");
        button.addActionListener(new ButtonLauscher());
        
        this.getContentPane().add(button);
    }
    
    class ButtonLauscher implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == button){
                System.out.println("Button geklickt!");
            }
        }
    }
    
    public static void main(String[] args){
        ButtonEvent2Class bec = new ButtonEvent2Class();
        bec.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        bec.setSize(200, 200);
        bec.setVisible(true);
    }
}