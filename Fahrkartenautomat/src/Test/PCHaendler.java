package Test;
import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		
		// Benutzereingaben lesen
	
		
		String artikel = liesString();
		
		int anzahl = liesInt();

		double preis = liesDouble();
		
		double mwst = liesDouble2();
		
		// Verarbeiten
														
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		
		rechungausgeben(artikel, anzahl, bruttogesamtpreis, mwst, nettogesamtpreis);

	}
	
	
	public static String liesString() {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("was m�chten Sie bestellen?");
		String artikel = myScanner.next();
		return artikel;
	}
	
	
	public static int liesInt() {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();
		return anzahl;
		
	}
	
	
	public static double liesDouble() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();
		return preis;
	}
	
	public static double liesDouble2() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();
		return mwst;
	}
	
	
	public static double berechneGesamtnettopreis(int anzahl, double
	nettopreis)
	{
		double nettoGesamntpreis;
		
		nettoGesamntpreis = anzahl * nettopreis;
		
		return nettoGesamntpreis;
		
	}
	
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis,
	double mwst) {
		
		double bruttogesamtPreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtPreis;
	}
	
	
	public static void rechungausgeben(String artikel, int anzahl, double
	nettogesamtpreis, double bruttogesamtpreis,
	double mwst) {
		
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	
	

}
