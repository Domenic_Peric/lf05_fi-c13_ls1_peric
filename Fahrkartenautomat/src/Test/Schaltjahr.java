import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		int jahresZahl = ermittleJahreszahl();
		boolean schaltjahr = pruefeJahreszahl(jahresZahl);
		ausgabe(schaltjahr, jahresZahl);
	}

	public static int ermittleJahreszahl() {
		boolean eingabePruefung = true;
		int jahresZahl = 1000;
		while(eingabePruefung) {
			System.out.print("Bitte geben Sie eine g�ltige Jahreszahl ein: ");
			Scanner nutzerEingabe = new Scanner(System.in);
			jahresZahl = nutzerEingabe.nextInt();
			if(jahresZahl >999 && jahresZahl <10000) {
				eingabePruefung = false;
			}
		}
		return jahresZahl;
	}
	

	public static boolean pruefeJahreszahl(int jahresZahl) {
		boolean schaltjahr = false;
		if(jahresZahl < 1582) {
			if(jahresZahl % 4 == 0) {
				schaltjahr = true;
			}
			else {
				schaltjahr = false;
			}
		}
		else {
			if(jahresZahl % 4 == 0) {
				schaltjahr = true;
				if(jahresZahl % 100 == 0 && jahresZahl % 400 != 0) {
					schaltjahr = false;
				}
			}	
		}
		return schaltjahr;
	}

	public static void ausgabe(boolean schaltjahr, int jahresZahl) {
		if(schaltjahr == false) {
			System.out.println("Das Jahr " + jahresZahl + " ist kein Schaltjahr.");
		}
		else {
			System.out.println("Das Jahr " + jahresZahl + " ist ein Schaltjahr.");			
		}
	}
	
}