
public class Ausgabe2 {

	public static void main(String[] args) {
		System.out.println("Guten Tag, Domenic");
		
		System.out.println("Willkommen in der Veranstaltung Strukturierte Programmierung.");
		
		System.out.println("Hier geht es dann in der selben Zeile weiter"); 
		
		//Der Unterschied zwischen print und println ist, das println die Zeile beendet
		
		System.out.printf("Hello %s! \n", "World");
		
		System.out.printf( "%20s\n", "Sammelbetrag" );

	}

}
