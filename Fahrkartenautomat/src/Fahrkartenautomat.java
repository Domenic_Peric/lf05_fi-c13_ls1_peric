﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       
       double rückgabebetrag;
       double endBetrag;
       

       
       endBetrag = fahrkartenbestellungErfassen();
       rückgabebetrag = fahrkartenBezahlen(endBetrag);
       
       fahrkartenAusgeben();
       
       rueckgeldAusgeben(endBetrag, rückgabebetrag);  }
    
	public static double fahrkartenbestellungErfassen() {
				Scanner myScanner = new Scanner(System.in);

					double[] ticket = {0, 3.0, 3.30, 3.60, 1.9, 8.60, 9.00,
							 9.60, 23.50, 24.30, 24.90, 0};
				boolean kauf = true;
					
				double gesamtpreis = 0;
				
					while(kauf ==true) {
										
						System.out.print("welche Fahrkarte wollen sie? 		 \r\n"
								+ "		 1	Einzelfahrschein Berlin AB 		 3,00\r\n"
								+ "		 2	Einzelfahrschein Berlin BC		 3,30\r\n"
								+ "		 3	Einzelfahrschein Berlin ABC		 3.60\r\n"
								+ "		 4	Kurzstrecke				 1.90\r\n"
								+ "		 5	Tageskarte Berlin AB			 8,60\r\n"
								+ "		 6	Tageskarte Berlin BC			 9,00\r\n"
								+ "		 7	Tageskarte Berlin ABC			 9,60\r\n"
								+ "		 8	Kleingruppen-Tageskarte Berlin AB	23,50\r\n"
								+ "		 9	Kleingruppen-Tageskarte Berlin BC	24,30\r\n"
								+ "		 10	Kleingruppen-Tageskarte Berlin ABC	24,90\r\n"
								+ "		 11 	Bezahlen\n");
						
						int tic1 = myScanner.nextInt();
												
					if (tic1 == 11) {
									kauf = false;}
												
											
					else {
									System.out.print("Wie viele dieser Karten möchten sie?");
									
									int tic2 = myScanner.nextInt();
									int anzahlrohlinge = 20;

									double ticketpreis = ticket[tic1] * tic2;
																	
									gesamtpreis = gesamtpreis + ticketpreis;
										
									System.out.printf("der momentane Preis beträgt: %.2f\n", gesamtpreis);  
								 anzahlrohlinge -= tic2; }

					      }
					return (gesamtpreis);}
    	
	public static double fahrkartenBezahlen(double a) {
		Scanner myScanner = new Scanner(System.in);
		
		
		   double eingezahlterGesamtbetrag;
	       double eingeworfeneMünze;
	       
	       eingezahlterGesamtbetrag = 0.0;
	       
	       
	       while(eingezahlterGesamtbetrag < a){
	    	   
		
			    	   System.out.printf("Noch zu zahlen: %.2f Euro \n" , (a - eingezahlterGesamtbetrag));
			    	   
			    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro):" );
			    	   
			    	   					eingeworfeneMünze = myScanner.nextDouble();
			           
			    	   					eingezahlterGesamtbetrag += eingeworfeneMünze;
	       	}
	       return eingezahlterGesamtbetrag;

	}
	
	public static void fahrkartenAusgeben() {
    System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       try {	
			Thread.sleep(250);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
       
    }System.out.println("\n\n");
    }
		
	public static void rueckgeldAusgeben(double b, double c) {

	Scanner myScanner = new Scanner(System.in);
	
       double rückgabebetrag;
       


       rückgabebetrag = c - (b);
       if(rückgabebetrag > 0.0) {
	    	  
    	   
    	   int[] geldspeicher = {4, 2, 1, 4, 20, 10};
    	   
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro \n" , rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
	
			double[] wertgeld = {1.999 , 0.999 , 0.499 , 0.1999 , 0.0999 , 0.04999 };     
			String[] münzrückgeld = {"    * * *\r\n"
						+ "  *       *\r\n"
						+ " *    2    *\r\n"
						+ " *   Euro  *\r\n"
						+ "  *       *\r\n"
						+ "    * * *\r\n"
						+ "    * * *\r\n", 
	   						 "  *       *\r\n"
	   						+ " *    1    *\r\n"
	   						+ " *   Euro  *\r\n"
	   						+ "  *       *\r\n"
	   						+ "    * * *\r\n"
	   						, "    * * *\r\n"
	   						+ "  *       *\r\n"
	   						+ " *    50    *\r\n"
	   						+ " *   Cent  *\r\n"
	   						+ "  *       *\r\n"
	   						+ "    * * *\r\n"
	   						, "    * * *\r\n"
	   						+ "  *       *\r\n"
	   						+ " *    20    *\r\n"
	   						+ " *   Cent  *\r\n"
	   						+ "  *       *\r\n"
	   						+ "    * * *\r\n"
	   						, "    * * *\r\n"
	   						+ "  *       *\r\n"
	   						+ " *    10    *\r\n"
	   						+ " *   Cent  *\r\n"
	   						+ "  *       *\r\n"
	   						+ "    * * *\r\n"
	   						, "    * * *\r\n"
	   						+ "  *       *\r\n"
	   						+ " *     5    *\r\n"
	   						+ " *   Cent  *\r\n"
	   						+ "  *       *\r\n"
	   						+ "    * * *\r\n"};
			           
	    	   	for(int i = 0; i <6; i++) { while(rückgabebetrag >= wertgeld[i] && geldspeicher[i] > 0) // 2 EURO-Münzen
			           {
			        	   System.out.print( münzrückgeld[i]);
			        	   geldspeicher[i]--;
						          rückgabebetrag -= wertgeld[i]; }
	    	   	}
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt."
                          + "\n\n\n\n");
       
       
       System.out.print("Neuer Kauf?");
       String einkauf = myScanner.next();
       
       if (einkauf.equalsIgnoreCase("ja")) {
     
        String[] args = null;
		main(args);
       }
       else {System.exit(0);}
	
	
	}

}
