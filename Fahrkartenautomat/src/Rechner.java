import java.util.Scanner;
		
		public class Rechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				
				Scanner myScanner = new Scanner(System.in);
				
				System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

				 // Die Variable zahl1 speichert die erste Eingabe
				 double zahl1 = myScanner.nextDouble();

				 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

				 // Die Variable zahl2 speichert die zweite Eingabe
				 double zahl2 = myScanner.nextDouble();

				 // Die Addition der Variablen zahl1 und zahl2
				 // wird der Variable ergebnis zugewiesen.
				 double ergebnis = zahl1 + zahl2;

				 System.out.print("\n\n\nErgebnis der Addition lautet: ");
				 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
				 
				 System.out.print("\n\n\nErgebnis der Substraktion lautet: ");
				 double ergebnis1 = zahl1 - zahl2;
				 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis1);
				 
				 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
				 double ergebnis2 = zahl1 * zahl2;
				 System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis2);
				 
				 System.out.print("\n\n\nErgebnis der Division lautet: ");
				 double ergebnis3 = zahl1 / zahl2;
				 System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis3);
				 myScanner.close();
	}

}
